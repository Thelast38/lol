#ifndef USER_PAGE_H
#define USER_PAGE_H

#include <QWidget>
#include "user_info_db.h"
namespace Ui {
class User_page;
}

class User_page : public QWidget
{
    Q_OBJECT

public:
    explicit User_page(QWidget *parent = nullptr);
    ~User_page();

private:
    Ui::User_page *ui;
};

#endif // USER_PAGE_H
