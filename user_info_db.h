#ifndef USER_INFO_DB_H
#define USER_INFO_DB_H
#include <QFile>
#include <QTextStream>
#include <string>
#include<QtDebug>
#include "time.h"
#include <iostream>
#include <chrono>
#include <ctime>
#include <set>
class user_info_db
{
public:
    void setTempl(QString Theme,QString Author,QString article_title);
    explicit user_info_db(QString file);
    void add_newData();
    QString return_value(QString key);
    QString return_value(QString key,QString value,QString input_key);
    QString return_line(QString key,QString value);
    void delete_line(QString key,QString value);
    void change_line(QString key,QString value,QString value1);
    void change_line(QString key,QString value,QString key1,QString value1);
    QString QByteArray_to_QString(QByteArray templ);
    int return_value_data();//возврашает колличество записей
private:

    QString Templ;
    QFile *db;
    QString Login_data[2];
    /*  QString login;
        QString hash_password;
        */
};

#endif // USER_INFO_DB_H
