#ifndef ADMIN_PAGE_H
#define ADMIN_PAGE_H

#include <QWidget>

namespace Ui {
class Admin_page;
}

class Admin_page : public QWidget
{
    Q_OBJECT

public:
    explicit Admin_page(QWidget *parent = nullptr);
       bool check(QString check);
    ~Admin_page();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Admin_page *ui;
};

#endif // ADMIN_PAGE_H
