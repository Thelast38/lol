#include "user_page.h"
#include "ui_user_page.h"

User_page::User_page(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::User_page)
{
    user_info_db db("data.txt");
    ui->setupUi(this);
    int a =db.return_value_data();
    ui->tableWidget->setRowCount(a);
    ui->tableWidget->setColumnCount(3);
    QString Them = db.return_value("Theme");
    QString buffer;
    int buff=0;
    for (int i=0;i<Them.length();i++) {
        if(Them[i] == "|"){
            ui->tableWidget->setItem(buff,0,new QTableWidgetItem(buffer));
            buff++;
            buffer.clear();
        }
        else {
           buffer+=Them[i];
        }

    }
    buffer.clear();
    buff=0;
    Them = db.return_value("Author");
    for (int i=0;i<Them.length();i++) {
        if(Them[i] == "|"){
            ui->tableWidget->setItem(buff,1,new QTableWidgetItem(buffer));
            buff++;
            buffer.clear();
        }
        else {
            buffer+=Them[i];
        }

    }

    buffer.clear();
    buff=0;
    Them = db.return_value("article_title");
    for (int i=0;i<Them.length();i++) {
        if(Them[i] == "|"){
            ui->tableWidget->setItem(buff,2,new QTableWidgetItem(buffer));
            buff++;
            buffer.clear();
        }
        else {
            buffer+=Them[i];
        }
    }

}

User_page::~User_page()
{
    delete ui;
}
