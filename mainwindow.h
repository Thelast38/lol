#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "user_page.h"
#include "admin_page.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    
    void on_add1_clicked();

private:
    Admin_page *notlol;
    User_page *lol;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
